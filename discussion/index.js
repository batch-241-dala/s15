console.log("Hello World");

// Syntax of console.log: console.log(variableOrStringToBeConsoled);

// It will comment parts of the code that gets ignored by the language

/*
	There are two types of comments
	1. the single line comment denoted by two slashes
	2. the multi-line comment denoted by slash and asterisk
*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform
	// JS statements usually end with semicolon (;)
	// Semicolons are not required in JS but we will use it to help us train to locate where statement ends.
	// A syntax in programming, it is the set of rules that we describes statements must be constructed.
	// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.

// [Section] Variable
	// Variables are used to contain data
	// Any information that is used by an application is stored what we call the memory
	// when we create variables, certain portions of device's memory is given a name that we call variables

	// This makes it easier for us to associate information stored in our devices to actual "names" about information
	
	// Declaring Variables
	// Declaring Variables - it tells our devices that a variable name is created and is ready to store data.
		// Syntax
			/*let/const variableName;*/

let myVariable ="Ada Lovelace";
let variable;
// This will caused an undefined variable because the declared variable does not have initial value
console.log(variable);
// Const keyword is used when the value of the variable won't change.
const constVariable ="John Doe";
// console.log() is useful for printing values of variables or certain results of code into the browser's console.
console.log(myVariable);
console.log(constVariable);

/*
	Guides in writing Variables
		1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
		2. Variable names should start with lowercase character, use camelCasing for the multiple words.
		3. for constant variables, use the 'const keyword'
			Note: If we use const keyword in declaring a variable, we cannot change the value of its variable.
		4. Variable names shoud be indicative(descriptive) of the value being stored to avoid confusion.
*/

// Declare and initialize
	// Initializing variables - the instance when a variable is given to its first/initial value.
	// Syntax:
		//  let/const variableName = initial value;

// Declaration and Initialization of the variable occur
let productName = "desktop computer";
console.log(productName);

// Declaration 
let desktopName;
console.log(desktopName);
// Initialization of the value of variable desktopName
desktopName = "Dell";
console.log(desktopName);

// Reassigning value
	// Syntax: variableName = newValue;
productName = "Personal Computer";
console.log(productName);

const name = "Chris";
console.log(name);

// This reassignment will cause an error since we cannot change/reassign the initial value of constant variable
/*name = "Topher";
console.log(name);*/

// This will cause an error on our code because the productName variable is already taken.

	/*let productName = "Laptop";*/


lastName = "Mortel";
console.log(lastName);


/*
	using Initialization first before declaration is a bad practice

batch = "Batch 241";
console.log(batch);

*/

////////////////
// let/const local/global scope
/*
	Scope essentially means where these variables are available for use
	
	let/const are block scope

	A block is a chunk of code bounded by {}.

*/

/*let outerVariable = "Hello";
{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}
console.log(outerVariable);

*/

/*const outerVariable = "Hello";
{
	const innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);

// Multiple Variable declarations

let productCode = "DC017" , productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// Using a variable with a reserved keyword is not allowed

// [Section] Data Types

// Strings
/*
	Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating a text
	Strings in JavaScript can be written using either a single quote ('') or double quote ("")
*/

let country = "Philippines";
let province = 'Metro Manila';
console.log(country);
console.log(province);

// Concatenate strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

// "\n" - refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Numbers
// Integer/Whole Number

let headCount = 27;
console.log(headCount);
console.log(typeof headCount);

// Decimal Number
let grade = 98.7;
console.log(grade);
console.log(typeof grade);


// Exponential Notation
let planetDistance = "2e10";
console.log(planetDistance);
console.log(typeof planetDistance);


console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// true or false

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// Arrays
// Arrays are special kind of data type that is used to store multiple values

/*
	Syntax:
	let/const arrayName = [elementA, elementB, ...];
*/

// similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);

let details = ["John", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items

/*
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}

*/

let person = {

	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["+639123456789", "8123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fourthGrading: 94.7
}
console.log(myGrades);

/*
	Constant Objects and Arrays

	We can change the element of an array to a constant variable
*/

const anime = ["one piece", "one punch man", "your lie in April"];
console.log(anime);

anime[0] = ['kimetsu no yaiba'];
console.log(anime);

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;
console.log(spouse);


let myNumber = 0;
let myString = "";

// Undefined
// Represent the state of a variable that has been declared but without an assigned value
let inARelationship;
console.log(fullName);

